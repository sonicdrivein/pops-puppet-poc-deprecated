class hellowindows::motd {
	
	file { "C:\\temp\\motd.txt":
		owner 	=> 'administrator',
		group 	=> 'administrators',
		content => "Hello, Windows!\n",
	}
}